import telegram


class Engine():
    """
    Creates engine object and proceed essential methods
    """
    inited = False

    @staticmethod
    def init(bot, update):
        """
        Init static members for entire class
        """
        Engine.bot = bot
        Engine.chat_id = update.message.chat_id
        Engine.sender_name = update.message.from_user.first_name
        Engine.inited = True

        Engine.bot.setWebhook()
    
    def prepare_records_list_message(
        self, records, mark_completed=False, keyword=''):
        message = ''
        for record in records:
            # add category marker for general records output
            if len(record.category):
                message += "<b>{0}:</b> ".format(record.category)
            if mark_completed:
                if record.active is False:
                    message += "<i>[Виконано]</i> "
            if len(keyword):
                hihglighted_text = record.text.replace(keyword, "<b>{0}</b>".format(keyword))
                message += '<b>{0}.</b> {1} \n\n'.format(record.record_id, hihglighted_text)
            else:
                message += '<b>{0}.</b> {1} \n\n'.format(record.record_id, record.text)
        return message

    def prepare_categories_list_message(self, categories):
        message = ''
        for cat in categories:
            message += '<b>{0}</b> \n'.format(cat)
        return message

    def send(self, message):
        """
        Send proeeded message
        """
        message_list = []
        if message is '':
            message = 'Жодних записів за цим запитом.'
        # handle big messages:
        while len(message) > 4095:
            message_list.append(message[0:4095])
            message = message[4095:]
        message_list.append(message)
        message_list.reverse()

        while len(message_list) > 0:
            self.bot.sendMessage(
                chat_id=self.chat_id, text=message_list.pop(), parse_mode=telegram.ParseMode.HTML)
